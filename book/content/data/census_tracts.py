# %% [markdown]
"""
# Census tract shapefiles

I get census tract shapefiles for New York State from the US Census Bereau data [site](https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html). The ftp url for New York State census tract shapefiles is https://www2.census.gov/geo/tiger/TIGER2020/TRACT/tl_2020_36_tract.zip.
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
import geopandas as gpd
import folium
import os
from pygsutils import general as g

# %%
def fp_nys_census_tract() -> str:
    url = "https://www2.census.gov/geo/tiger/TIGER2020/TRACT/tl_2020_36_tract.zip"
    loc = "./../../data"
    os.makedirs(loc, exist_ok=True)
    fp = f"{loc}/nys_census_tracts.zip"
    if not os.path.isfile(fp):
        print(f"downloading census tracts to {fp}")
        g.download(url, fp)
    return fp


def queens_census_tracts() -> gpd.GeoDataFrame:
    return gpd.read_file(fp_nys_census_tract()).query("COUNTYFP == '081'")


# %%
m = folium.Map(location=[40.719, -73.845], tiles="Stamen Toner", zoom_start=13)

# %%
folium.GeoJson(
    queens_census_tracts().to_json(), style_function=lambda x: {"fillColor": "orange"}
).add_to(m)

# %%
m