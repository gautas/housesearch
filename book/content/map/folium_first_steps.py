# %% [markdown]
"""
# First steps with folium
I'm checking out [folium](https://python-visualization.github.io/folium/) to see if I can use for a map of NYC neighborhoods to show some of the statistics that we care about (e.g. school demographics). These are my first steps trying out the library and seeing how it renders in jupyter book.
"""

# %% tags=['hide-cell']
from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')

# %%
import folium

# %%
m = folium.Map(location=[40.719, -73.845], tiles="Stamen Toner", zoom_start=13)

# %%
m
# %%
