# %% [markdown]
"""
# Map of demographics
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from housesearch.tracts import base as tb
import folium
from folium.plugins import Fullscreen
from folium.features import GeoJsonTooltip
import branca
import json
import altair as alt
import pandas as pd

# %%
sfs = tb.Shapefiles()
pop = tb.Population()

# %%
m = folium.Map(location=[40.719, -73.845], tiles="Stamen Toner", zoom_start=13)

# %%
gdf_queens = sfs.gdf_queens

# %%
gdf_queens["tract_simp"] = gdf_queens["TRACTCE"].map(lambda x: x[:-2] + "00")
gdf_queens_dissolve = gdf_queens.dissolve(by="tract_simp", as_index=False)

# %%
colorscale = branca.colormap.linear.Purples_09.scale(0, 1)

# %%
def style_function(feature):
    df = pop.df_long.query(f"tract_simp == '{feature['properties']['tract_simp']}'")
    if len(df) == 0:
        print(feature['properties']['tract_simp'])
    # print(df)
    black_prop = df.query("year == 2019").query("race == 'black_prop'")["prop"].values[0]
    return {
        "fillOpacity": 0.9,
        "weight": 0.5,
        "fillColor": "#black" if pd.isna(black_prop) else colorscale(black_prop),
    }

# %%
# following https://stackoverflow.com/questions/54595931/show-different-pop-ups-for-different-polygons-in-a-geojson-folium-python-ma
layer = folium.FeatureGroup(name="pop_over_time", show=False)
temp_geojson = json.loads(gdf_queens_dissolve.to_json())

# %%
for feature in temp_geojson["features"]:
    temp_layer = folium.GeoJson(feature, style_function=style_function)
    tract_simp = feature["properties"]["tract_simp"]
    popup = folium.Popup(tract_simp)
    df = pop.df_long[pop.df_long["tract_simp"] == tract_simp]
    chart = (
        alt.Chart(df).transform_filter(alt.FieldOneOfPredicate("race", ["white_prop", "black_prop", "asian_prop"]))
       .mark_line()
         .encode(x="year:O", y=alt.Y("prop", axis=alt.Axis(format="%"), scale=alt.Scale(domain=(0,1))), color="race")
    )
    chart_json = json.loads(chart.to_json())
    folium.features.VegaLite(chart_json).add_to(popup)
    temp_layer.add_child(popup)
    temp_layer.add_to(layer)
layer.add_to(m)

# %%
Fullscreen().add_to(m)

# %%
m
# %%
