"""base tools for working with census tracts
"""

from dataclasses import dataclass
import os
from pygsutils import general as g
import geopandas as gpd
import logging
from functools import cached_property
import requests
import json
from typing import Dict, List
import pandas as pd

loc = os.path.dirname(__file__)


@dataclass
class Shapefiles:
    @property
    def url(self) -> str:
        return "https://www2.census.gov/geo/tiger/TIGER2020/TRACT/tl_2020_36_tract.zip"

    @property
    def fp(self) -> str:
        return f"{loc}/nys_census_tracts.zip"

    @cached_property
    def gdf(self) -> gpd.GeoDataFrame:
        if not os.path.isfile(self.fp):
            logging.info(f"downloading {self.url} to {self.fp}")
            g.download(self.url, self.fp)
        return gpd.read_file(self.fp)

    @cached_property
    def gdf_queens(self) -> gpd.GeoDataFrame:
        return self.gdf.query("COUNTYFP == '081'")


@dataclass
class VariableCensus:
    name: str
    desc: str


variables_census = [
    VariableCensus(name="B02001_001E", desc="total"),
    VariableCensus(name="B02001_002E", desc="white"),
    VariableCensus(name="B02001_003E", desc="black"),
    VariableCensus(name="B02001_004E", desc="am_ind"),
    VariableCensus(name="B02001_005E", desc="asian"),
    VariableCensus(name="B02001_006E", desc="pi"),
    VariableCensus(name="B02001_007E", desc="other"),
    VariableCensus(name="B02001_008E", desc="multi"),
]

races = ["white", "black", "am_ind", "asian", "pi", "other", "multi"]

years = [2019, 2018, 2017, 2016, 2015]


@dataclass
class PopulationYear:
    year: str

    @property
    def url_api(self) -> str:
        return f"https://api.census.gov/data/{self.year}/acs/acs5?get=NAME,group(B02001)&for=tract:*&in=state:36&in=county:081"

    @property
    def fp_json(self) -> str:
        return f"{loc}/census_{self.year}.json"

    @cached_property
    def content(self) -> str:
        print(self.url_api)
        with requests.get(self.url_api) as r:
            content = r.content
        return content

    @cached_property
    def json(self) -> List:
        if not os.path.isfile(self.fp_json):
            with open(self.fp_json, "w") as f:
                json.dump(json.loads(self.content), f)
        with open(self.fp_json, "r") as f:
            _json = json.load(f)
        return _json

    @cached_property
    def df(self) -> pd.DataFrame:
        return pd.DataFrame(self.json[1:], columns=self.json[0]).rename(
            columns={var.name: var.desc for var in variables_census}
        )

    @cached_property
    def df_tract_simp(self) -> pd.DataFrame:
        df = self.df
        df["tract_simp"] = df["tract"].map(lambda x: x[:-2] + "00")
        return df.groupby(["tract_simp"], as_index=False).agg(
            {i: "sum" for i in races + ["total"]}
        )

    @cached_property
    def df_w_prop(self) -> pd.DataFrame:
        df = self.df_tract_simp
        for i in races:
            df[f"{i}_prop"] = df[i].astype(float) / df["total"].astype(float)
        return df

    @cached_property
    def df_long(self) -> pd.DataFrame:
        return self.df_w_prop.melt(
            id_vars=["tract_simp"],
            value_vars=[f"{i}_prop" for i in races],
            var_name="race",
            value_name="prop",
        ).assign(year=self.year)


@dataclass
class Population:
    @property
    def population_years(self) -> List[PopulationYear]:
        return [PopulationYear(year=year) for year in years]

    @cached_property
    def df_long(self) -> pd.DataFrame:
        return pd.concat([pop_year.df_long for pop_year in self.population_years])