import setuptools


with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="housesearch",
    version="0.0.1",
    author="Gautam Sisodia",
    packages=setuptools.find_packages(),
    classifiers=["Progamming Language :: Python :: 3"],
    install_requires=[
        "pygsutils @ git+http://github.com/gautsi/pygsutils#egg=pygsutils",
        "folium==0.12.1",
        "ipython==7.22.0",
        "geopandas==0.9.0",
        "matplotlib==3.4.1",
        "altair==4.1.0",
    ],
)
